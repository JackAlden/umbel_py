import requests
import json
import sys
from termcolor import colored

sys.setrecursionlimit(1100)

# Setup
game_url          = 'https://umbelmania.umbel.com/training/'
head              = {'content-type': 'application/json'}
payload           = {'opponent': 'pato-bajo-jr', 'player_name': 'Steven_Esquelito', 'email': 'sirnapier88@gmail.com'}
game_initializer  = requests.post(game_url, headers=head, data=json.dumps(payload))
moves             = requests.get('https://umbelmania.umbel.com/moves/').json()

countdown_to = 997

# Method for game play
def play_game(game_info):
    # Convert response to python dictionary
    info_py     = game_info.json()
    
    # Setup variables for readability
    moves_left  = info_py['gamestate']['moves_remaining']
    index       = 1000 - moves_left
    
    # Create next move
    info_py['move'] = choose_move(payload['opponent'], moves_left)
      
    # Convert submission to JSON
    info_j = json.dumps(info_py)
    
    if moves_left > countdown_to:
      # Set next_round variable to response from submission
      next_round    = requests.post(game_url, headers=head, data=info_j)
      
      # Show moves submitted to figure out patterns
      scoreboard    = next_round.json()['gamestate']
      opponent_move = scoreboard['opponent_moves'][index]
      my_move       = scoreboard['your_moves'][index]
      current_score = scoreboard['total_score']
      seed_val      = scoreboard['seed']
      
      info_print(my_move, opponent_move, seed_val, moves_left, current_score)
      
      play_game(next_round)
    
    elif moves_left == 0:
      print info_py['gamestate']['total_score']
    
# Choosing moves
def choose_move(o, moves):
  if o == 'pato-bajo-jr':
    check = moves % 10
    if check == 9:
      return 'H'
    if check == 8:
      return 'I'
    if check == 7:
      return 'J'
    if check == 6:
      return 'K'
    if check == 5:
      return 'A'
    if check == 4:
      return 'B'
    if check == 3:
      return 'C'
    if check == 2:
      return 'D'
    if check == 1:
      return 'E'
    if check == 0:
      return 'G'
  if o == 'princesa-comico':
    return 'A'
  if o == 'el-rey-muy-dante':
    check = moves % 11
    if check in range(-1,2) or check == 5 or check in range(8,11):
      return 'K'
    if check in range(3,5) or check in range(6,8):
      return 'F'
    else:
      return 'A'
  if o == 'senor-amistoso':
    return 'A'

# Methods for output
def win_or_lose(mine, theirs):
  return "win" if theirs in moves[mine]['beats'] else "loss"

def graph(user_move):
  return " " * -(65 - ord(user_move)) + user_move + " " * (75 - ord(user_move))
  
def info_print(mine, theirs, seed, left, score):
  color     = "green" if win_or_lose(mine, theirs) == "win" else "red"
  from_zero = -(65 - ord(theirs))
  
  output           = "%s vs. %s | %s | %s | %s" % (mine, theirs, graph(mine), graph(theirs), left % 10)
  output_c         = "moves remaining: %04d | score: %04d" % (left, score)
  print colored(output, color)

# Play the game!
play_game(game_initializer)


